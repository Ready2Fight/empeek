$(window).scroll(function() {

	// 300 = The point you would like to fade the scroll button.

	if ($(window).scrollTop() > 300 ){

	//ISSUE: don't faiding slow, it happing instantly WHY?
		$('.js-scroll-to').fadeIn('slow ease-in');

	} else {

		$('.js-scroll-to').fadeOut('slow ease-in');

	};
});

$('.js-scroll-to').on('click', function(e){
	e.preventDefault();

	$('html, body').animate({
		scrollTop: $(this.hash).offset().top
	}, 1500);
});
